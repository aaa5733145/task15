﻿#include <iostream>
#include <string>
#include <algorithm> 
 using namespace std;

class Player
{
private:
    std::string name;
    double points;


public:
    Player()
    {}
    Player(std::string _name, double _points): name(_name),points(_points)
    {}
    void Set_Name(std::string x)
    {
        name = x;
    }
    void Set_Points(double x)
    {
        points = x;
    }
    std::string Get_Name()
    {
        return name;
    }
    double Get_Points()
    {
        return points;
    }
   friend bool points_sorter(const Player&,const  Player&);
};

bool points_sorter(const Player& l, const Player& r)
{
    return l.points > r.points;
}


int main()
{
    system("chcp 1251 > nul");
    int countofplayers;
    std::cout << "How manu players you want!?\n";
    std::cin >> countofplayers;
    Player* players = new Player[countofplayers];
    for (int i = 0; i < countofplayers; ++i)
    {
        std::string name;
        double points;
        std::cout << "Enter name and points for player on number " << i << ":"<<"\n";
        std::cout << "Name:";
        std::cin >> name;
        std::cout << "Points:";
        std::cin >> points;
        players[i].Set_Name(name);
        players[i].Set_Points(points);
    }
    sort(players, players + countofplayers, points_sorter);
    for (int i = 0; i < countofplayers; i++)
    {
        std::cout<< players[i].Get_Name()<<" "<< players[i].Get_Points()<<"\n";
    }

    delete[] players;
}

